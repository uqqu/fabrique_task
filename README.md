# fabrique_task



## Требуется

python 3.8+

-> requirements.txt

celery

rabbitMQ

## Запуск

Применить миграции бд, создать суперпользователя, запустить rabbitmq сервер, запустить celery worker, manage.py runserver

## API
```
> api/v1/clientslist/
- get – список клиентов
    {
        имя
        номер телефона
        время начала приёма сообщений
        время окончания приёма сообщений
        часовой пояс
        метки
        количество полученных сообщений
    }
- post – создать клиента
    {
        "name" str:64
        "phone" str:11; r'7\d{10}'
        ["timezone" default=UTC]
        ["start_receive_time" time]
        ["end_receive_time" time]
        ["tags" list[str]]
    }
- head
- options
> api/v1/clientslist/<int:id>/
- get – информацию о клиенте (аналогично общему списку)
- put – заменить клиента
- patch – обновить некоторые поля
- delete – удалить клиента
- head
- options
> api/v1/mailingslist/
- get – список рассылок
    {
        дата-время начала рассылки
        дата-время окончания рассылки
        статус
        текст сообщений
        метки
        коды операторов
        количество успешно отправленных сообщений
        количество безуспешно отправленных сообщений
    }
- post – создать рассылку (если метки и операторы пустые – рассылка запускается для всех клиентов)
    {
        "start_date" datetime
        "end_date" datetime
        "status" boolean
        "message" text
        ["tags" list[str]]
        ["operator_codes" list[str:3]]
    }
- head
- options
> api/v1/mailingslist/<int:id>/
- get – информация о рассылке (аналогично общему списку)
- put – заменить рассылку
- patch – обновить некоторые поля
- delete – удалить рассылку
- head
- options
> api/v1/start_mailing/
- post – досрочно запустить рассылку
    {
        "id" int
    }

```

## OpenAPI/SwaggerUI
```
/docs/
```

## Дополнительные пункты

5. SwaggerUI на /docs/
6. Администраторский web-ui (manage.py createsuperuser)
9. Безуспешные отправки повторяются через 5 минут, пока не выйдет время получения
11. Сообщения отправляются с учётом локального времени клиента
12. Функции обработки рассылок и сообщений логгируют процесс
