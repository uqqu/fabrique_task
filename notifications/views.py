from django.forms import model_to_dict
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Clients, Mailings
from .serializers import ClientsSerializer, MailingsSerializer
from .tasks import start_mailing


class ClientsViewSet(viewsets.ModelViewSet):
    queryset = Clients.objects.all()
    serializer_class = ClientsSerializer


class MailingsViewSet(viewsets.ModelViewSet):
    queryset = Mailings.objects.all()
    serializer_class = MailingsSerializer


class ForcedMailingStart(APIView):
    def post(self, request):
        pk = request.data['id']
        start_mailing.apply_async((pk,))
        return Response({'new_mailing': model_to_dict(Mailings.objects.get(pk=pk))})
