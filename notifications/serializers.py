from rest_framework import serializers

from .models import Clients, Mailings, Operators, Tags
from .tasks import start_mailing


class ClientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clients
        fields = (
            'name',
            'phone',
            'start_receive_time',
            'end_receive_time',
            'timezone',
            'tags',
            'get_num_of_received_messages',
        )

    def to_internal_value(self, data):
        if 'tags' in data:
            tags = []
            for tag in data['tags']:
                tag, _ = Tags.objects.get_or_create(name=tag)
                tags.append(tag.pk)
            data['tags'] = tags
        return super().to_internal_value(data)

    def create(self, validated_data):
        operator, _ = Operators.objects.get_or_create(pk=validated_data['phone'][1:4])
        validated_data['operator_code'] = operator
        return super().create(validated_data)


class MailingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailings
        fields = (
            'start_date',
            'end_date',
            'status',
            'message',
            'tags',
            'operator_codes',
            'get_num_of_successful_messages',
            'get_num_of_failed_messages',
        )

    def to_internal_value(self, data):
        if 'tags' in data:
            tags = []
            for tag in data['tags']:
                tag, _ = Tags.objects.get_or_create(name=tag)
                tags.append(tag.pk)
            data['tags'] = tags

        if 'operator_codes' in data:
            codes = []
            for code in data['operator_codes']:
                code, _ = Operators.objects.get_or_create(code=code)
                codes.append(code.pk)
            data['operator_codes'] = codes
        return super().to_internal_value(data)

    def create(self, validated_data):
        mailing = super().create(validated_data)
        start_mailing.apply_async((mailing.id,), eta=mailing.start_date)
        return mailing
