from datetime import date, datetime, time
from zoneinfo import ZoneInfo
import logging

import requests
from celery import shared_task
from django.utils.timezone import now
from test_app.settings import API_TOKEN, API_URL

from .models import Clients, Mailings, Messages


@shared_task()
def start_mailing(mailing_id):
    logging.info(f'The mailing {mailing_id} started at {datetime.now()}')
    mailing = Mailings.objects.get(pk=mailing_id)
    clients = Clients.objects.all()
    if mailing.tags.all():
        clients = clients.filter(tags__in=mailing.tags.all())
    if mailing.operator_codes.all():
        clients = clients.filter(operator_code__in=mailing.operator_codes.all())
    for client in clients:
        client_time = datetime.now(ZoneInfo(client.timezone)).time()
        start = client.start_receive_time or time()
        end = client.end_receive_time or time(hour=23, minute=59, second=59)
        if start < client_time < end:
            send_message.apply_async((mailing.id, client.id))
        elif client_time < start:
            delta = datetime.combine(date.today(), start)
            delta -= datetime.combine(date.today(), client_time)
            send_message.apply_async((mailing.id, client.id), countdown=int(delta.total_seconds()))
    logging.info(f'{mailing_id=} queued up all the messages')


@shared_task()
def send_message(mailing_id, client_id):
    logging.info(f'The message from {mailing_id} to {client_id} treated at {datetime.now()}')
    mailing = Mailings.objects.get(pk=mailing_id)
    client = Clients.objects.get(pk=client_id)
    if (
        client.end_receive_time
        and now().time() > client.end_receive_time
        or now() > mailing.end_date
    ):
        logging.info(f'Invalid time to send for {client_id}. {datetime.now()}')
        return
    data = {'id': client.id, 'phone': int(client.phone), 'text': mailing.message}
    response = requests.post(API_URL, json=data, headers={'Authorization': API_TOKEN})
    if status := (response.status_code == 200):
        logging.info(f'Message for {client_id=} ({mailing_id=}) sent successfuly')
    else:
        logging.info(f'Message for {client_id=} ({mailing_id=}) sent failed. Retry in 5min.')
        send_message.apply_async((mailing_id, client_id), countdown=300)
    message = Messages(send_date=datetime.now(), status=status, mailing=mailing, client=client)
    message.save()
