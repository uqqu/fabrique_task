from django.contrib import admin
from django.contrib.auth.models import Group, User  # pylint: disable=E5142
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import Clients, Mailings, Messages, Operators, Tags


class __NoAddPermissionMixin:
    def has_add_permission(self, *_):
        return False


class ClientsAdmin(admin.ModelAdmin):
    def operator_link(self, obj):
        link = reverse('admin:notifications_operators_change', args=[obj.operator_code.pk])
        return mark_safe(f'<a href="{link}">{obj.operator_code}')

    operator_link.short_description = 'Оператор'  # type: ignore

    list_display = [field.name for field in Clients._meta.fields]
    list_display[list_display.index('operator_code')] = 'operator_link'
    list_display.append('get_num_of_received_messages')

    list_display_links = ['name']
    exclude = ('operator_code',)

    def save_model(self, request, obj, form, change):
        obj.operator_code, _ = Operators.objects.get_or_create(code=obj.phone[1:4])
        super().save_model(request, obj, form, change)


class MailingsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Mailings._meta.fields]
    list_display.extend(('get_num_of_successful_messages', 'get_num_of_failed_messages'))


class MessagesAdmin(__NoAddPermissionMixin, admin.ModelAdmin):
    pass


class OperatorsAdmin(__NoAddPermissionMixin, admin.ModelAdmin):
    list_display = ('code', 'name', 'get_num_of_clients')


class TagsAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_num_of_clients')


admin.site.register(Clients, ClientsAdmin)
admin.site.register(Mailings, MailingsAdmin)
admin.site.register(Messages, MessagesAdmin)
admin.site.register(Operators, OperatorsAdmin)
admin.site.register(Tags, TagsAdmin)

admin.site.unregister(Group)
admin.site.unregister(User)
