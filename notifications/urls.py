from django.urls import path
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from notifications.views import (ClientsViewSet, ForcedMailingStart,
                                 MailingsViewSet)

urlpatterns = [
    path(
        'docs/', get_schema_view(title="TestApp API"),
    ),
    path('api/v1/clientslist/', ClientsViewSet.as_view({'get': 'list', 'post': 'create'})),
    path(
        'api/v1/clientslist/<int:pk>/',
        ClientsViewSet.as_view(
            {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}
        ),
    ),
    path('api/v1/mailingslist/', MailingsViewSet.as_view({'get': 'list', 'post': 'create'})),
    path(
        'api/v1/mailingslist/<int:pk>/',
        MailingsViewSet.as_view(
            {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}
        ),
    ),
    path('api/v1/start_mailing/', ForcedMailingStart.as_view()),
]
