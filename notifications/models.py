import zoneinfo

from django.core.validators import RegexValidator
from django.db import models


class Mailings(models.Model):
    start_date = models.DateTimeField('Начало рассылки')
    end_date = models.DateTimeField('Окончание рассылки')
    status = models.BooleanField('Состояние', choices=((0, 'Остановлена'), (1, 'Запущена')))
    message = models.TextField('Текст сообщения')
    tags = models.ManyToManyField('Tags', verbose_name='Метки для рассылки', blank=True)
    operator_codes = models.ManyToManyField(
        'Operators', verbose_name='Операторы для рассылки', blank=True
    )

    def get_num_of_successful_messages(self):
        return self.messages_set.filter(status=True).count()

    def get_num_of_failed_messages(self):
        return self.messages_set.filter(status=False).count()

    get_num_of_successful_messages.short_description = 'Успешно отправлено сообщений'  # type: ignore
    get_num_of_failed_messages.short_description = 'Безуспешно отправлено сообщений'  # type: ignore

    def __str__(self):
        return f'{self.start_date}-{self.end_date} – {self.message}'

    class Meta:
        ordering = ('-start_date', 'end_date')
        verbose_name = 'рассылку'
        verbose_name_plural = 'Рассылки'


class Clients(models.Model):
    timezones = [(x, x) for x in zoneinfo.available_timezones()]
    phone_validator = RegexValidator(
        regex=r'7\d{10}',
        message='Некорректный номер. Введите номер из 11 цифр, начиная с 7, без "+"',
    )

    name = models.CharField('Имя', max_length=64)
    phone = models.CharField(
        'Номер телефона', max_length=11, validators=(phone_validator,), unique=True
    )
    operator_code = models.ForeignKey('Operators', on_delete=models.PROTECT)
    start_receive_time = models.TimeField('Время начала приёма сообщений', null=True, blank=True)
    end_receive_time = models.TimeField('Время окончания приёма сообщений', null=True, blank=True)
    timezone = models.CharField('Временная зона', max_length=32, choices=timezones, default='UTC')
    tags = models.ManyToManyField('Tags', verbose_name='Метки', blank=True)

    def get_num_of_received_messages(self):
        return self.messages_set.count()

    get_num_of_received_messages.short_description = 'Получено сообщений'  # type: ignore

    def __str__(self):
        if not self.start_receive_time:
            return f'{self.name} – {self.phone}'
        return f'{self.name} – {self.phone} ({self.start_receive_time}-{self.end_receive_time})'

    class Meta:
        ordering = ('phone',)
        verbose_name = 'клиента'
        verbose_name_plural = 'Клиенты'


class Messages(models.Model):
    send_date = models.DateTimeField()
    status = models.BooleanField()
    mailing = models.ForeignKey('Mailings', on_delete=models.CASCADE)
    client = models.ForeignKey('Clients', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.send_date} – {self.status} – {self.mailing} – {self.client}'

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'Сообщения'


class Tags(models.Model):
    name = models.CharField('Название', max_length=64, primary_key=True)

    def get_num_of_clients(self):
        return self.clients_set.count()

    get_num_of_clients.short_description = 'Количество клиентов'  # type: ignore

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'метку'
        verbose_name_plural = 'Метки'


class Operators(models.Model):
    code = models.CharField('Код', max_length=3, primary_key=True)
    name = models.CharField('Название', max_length=16, null=True)

    def get_num_of_clients(self):
        return self.clients_set.count()

    get_num_of_clients.short_description = 'Количество клиентов'  # type: ignore

    def __str__(self):
        return f'{self.code} – {self.name}'

    class Meta:
        verbose_name = 'оператора'
        verbose_name_plural = 'Операторы'
