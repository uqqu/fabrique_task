import os

from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_app.settings')

app = Celery('test_app')
app.config_from_object('django.conf:settings', namespace='CELERY')


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
